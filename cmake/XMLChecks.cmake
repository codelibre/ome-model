# #%L
# OME C++ libraries (cmake build infrastructure)
# %%
# Copyright © 2018 - 2019 Quantitative Imaging Systems, LLC
# Copyright © 2021 Roger Leigh
# %%
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are
# those of the authors and should not be interpreted as representing official
# policies, either expressed or implied, of any organization.
# #L%

if (POLICY CMP0067)
    cmake_policy(SET CMP0067 NEW)
endif()
if (POLICY CMP0075)
    cmake_policy(SET CMP0075 NEW)
endif()

include(CheckCXXSourceCompiles)

set(xmldom "xerces;qt6;qt5;qt" CACHE STRING "Use XML DOM classes types from 'xerces' (xerces and xalan) or 'qt6', 'qt5' or 'qt' (either Qt 6 or 5).  Multiple options will be used as fallbacks")

set(OME_XML_DOM NOTFOUND)
set(OME_XML_XSLT NOTFOUND)

message(STATUS "Checking XML DOM and XSLT implementations: ${xmldom}")

foreach(xd ${xmldom})
    if (xd STREQUAL "xerces")
        find_package(XercesC 3.0.0)
        if (XercesC_FOUND)
            # Xerces-C++ link test
            set(CMAKE_REQUIRED_INCLUDES_SAVE ${CMAKE_REQUIRED_INCLUDES})
            set(CMAKE_REQUIRED_INCLUDES ${CMAKE_REQUIRED_INCLUDES} ${XercesC_INCLUDE_DIRS})
            set(CMAKE_REQUIRED_LIBRARIES_SAVE ${CMAKE_REQUIRED_LIBRARIES})
            set(CMAKE_REQUIRED_LIBRARIES ${CMAKE_REQUIRED_LIBRARIES} ${XercesC_LIBRARIES})

            check_cxx_source_compiles(
                "#include <xercesc/util/PlatformUtils.hpp>

int main() {
  xercesc::XMLPlatformUtils::Initialize();
  xercesc::XMLPlatformUtils::Terminate();
}"
                    XERCES_LINK)

            if(NOT XERCES_LINK)
                message(WARNING "Xerces-C++ library link test failed")
            endif(NOT XERCES_LINK)

            set(CMAKE_REQUIRED_LIBRARIES ${CMAKE_REQUIRED_LIBRARIES_SAVE})
            set(CMAKE_REQUIRED_INCLUDES ${CMAKE_REQUIRED_INCLUDES_SAVE})
        endif()

        if (XercesC_FOUND AND XERCES_LINK)
            set(OME_XML_DOM TRUE)
            set(OME_HAVE_XERCES_DOM TRUE)
            message(STATUS "Using XML DOM: xerces")
        endif()
    endif()
    if (xd STREQUAL "qt6" OR xd STREQUAL "qt")
        find_package(Qt6Xml)
        find_package(Qt6Core5Compat)
        if (TARGET Qt6::Xml AND TARGET Qt6::Core5Compat)
            set(OME_XML_DOM TRUE)
            set(OME_HAVE_QT6_DOM TRUE)
            message(STATUS "Using XML DOM: qt6")
        endif()
    endif()
    if (xd STREQUAL "qt5" OR (xd STREQUAL "qt" AND NOT OME_XML_DOM))
        find_package(Qt5Xml)
        if (TARGET Qt5::Xml)
            set(OME_XML_DOM TRUE)
            set(OME_HAVE_QT5_DOM TRUE)
            message(STATUS "Using XML DOM: qt5")
        endif()
        find_package(Qt5XmlPatterns)
        if (TARGET Qt5::XmlPatterns)
            set(OME_XML_XSLT TRUE)
            set(OME_HAVE_QT5_XSLT TRUE)
            message(STATUS "Using XML XSLT: qt5")
        endif()
    endif()
    if(OME_XML_DOM)
        break()
    endif()
endforeach()

if(NOT OME_XML_DOM)
    message(FATAL_ERROR "No XML DOM implementation found")
endif()
