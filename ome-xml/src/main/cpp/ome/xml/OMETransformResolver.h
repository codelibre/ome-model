/*
 * #%L
 * OME-XML C++ library for working with OME-XML metadata structures.
 * %%
 * Copyright © 2016 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#ifndef OME_XML_MODEL_OMETRANSFORMRESOLVER_H
#define OME_XML_MODEL_OMETRANSFORMRESOLVER_H

#include <cstdint>
#include <filesystem>
#include <memory>
#include <ostream>
#include <set>
#include <string>
#include <utility>
#include <vector>

namespace ome
{
  namespace xml
  {

    class OMETransformResolverImpl;

    /**
     * Query available OME-XML transforms.
     *
     * This class will find the available XSL transforms to convert
     * between different OME-XML model versions, using either the
     * installed schemas, or schemas in a specified directory.
     *
     * In order to convert between different schema versions, one or
     * more transforms will require applying.  This class will
     * determine the optimal set of transforms to apply for a given
     * source and target model version, computed using a graph
     * representation and shortest path algorithm.
     */
    class OMETransformResolver
    {
    public:
      /**
       * Default constructor.
       *
       * The transform directory used will be the default set at
       * install time or in the environment.
       */
      OMETransformResolver();

      /**
       * Construct with specified transform directory.
       *
       * @param transformdir the directory containing the XSL
       * transform files.
       */
      OMETransformResolver(const std::filesystem::path& transformdir);

      /// Destructor.
      ~OMETransformResolver();

      /**
       * Get the available schema versions for transformation.
       *
       * @returns all schema versions found as a source or target for
       * a transform.
       */
      const std::set<std::string>&
      schema_versions() const;

      /**
       * Determine the optimal transform order between schema
       * versions.
       *
       * Run a shortest path search to determine the highest quality
       * set of transforms between the two versions.
       *
       * The returned quality metric is the sum of the quality metrics
       * for all of the transforms, rounded down to the nearest
       * quality value.
       *
       * @param source the source schema version.
       * @param target the target schema version.
       * @returns a vector of Transform objects in the order to apply,
       * and an aggregate quality metric.
       */
      std::vector<std::filesystem::path>
      transform_order(const std::string& source,
                      const std::string& target) const;

    private:
      /// Transform directory.
      std::filesystem::path transformdir;
    };

  }
}

#endif // OME_XML_MODEL_OMETRANSFORMRESOLVER_H

/*
 * Local Variables:
 * mode:C++
 * End:
 */
